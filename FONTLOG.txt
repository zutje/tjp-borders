FONTLOG
TJP Borders font family
========================

This file provides detailed information on the TJP Borders family of fonts. This
information should be distributed along with the TJP Borders fonts and any
derivative works.


Basic Font Information
----------------------
TJP Borders Light and TJP Borders Medium are the two fonts of TJP Borders. These
fonts were made to be able to construct borders in your documents. The font
consists of squares with a part of a border on them. The layout is such that the
blocks

qwe  rty  u o
asd  fgh  
zxc  vbn  m .

have the borders in the outward direction, with the central letter having a
filling form. These blocks can be used both small and capital and small and
capital letters can be used through each other. See the fontoverview files.

A special note should be given to the <TAB> key. This key can be used for
spacing only when the tab size matches the full font height. The font height
is 120% of the font size. I recommend using spaces though, because they are
exactly the right size.

The design files are included in the distribution


ChangeLog
---------
(This should list both major and minor changes, most recent first.)

24-04-2022 (Tjeerd J. Pinkert)  TJP Borders - Light, version 001.001
- Release as OpenType Font

11-07-2008 (Tjeerd J. Pinkert)  TJP Borders, version 001.000
- Light and Medium fonts ready for public release.
- First public release.

09-07-2008 (Tjeerd J. Pinkert)  TJP Borders - Medium, version 001.000
- Decision to create a medium font and bold font (not ready yet).
- Decision to create this as a font family.

19-05-2006 (Tjeerd J. Pinkert)  TJP Borders, version 001.000
- Font creation.


Information for Contributors
----------------------------
When contributing to this font, please send me an e-mail with your contri-
bution, or create a merge request on gitlab. Include a statement that your
contribution is licensed under the same license as the font (OFL v1.1).


Acknowledgements
----------------
(Here is where contributors can be acknowledged. If you make modifications
be sure to add your name (N), email (E), web-address (W) and description
(D). This list is sorted by last name in alphabetical order.)

N: Tjeerd J. Pinkert
E: t.j.pinkert@alumnus.utwente.nl
W: None
D: Original designer of the TJP Borders font
