##
## TJP Borders Light and Medium Makefile...
##

# Destination directory and Prefix for install scripts...
DESTDIR=
prefix=/usr/local

# Installation directory
INSTALL_FONT=${DESTDIR}${prefix}/share/fonts/opentype/borders
INSTALL_DOC=${DESTDIR}${prefix}/share/doc/font-borders

# version of the font to make / install
MAJOR_VERSION=001
MINOR_VERSION=001
VERSION=${MAJOR_VERSION}.${MINOR_VERSION}

.PHONY: install install-doc uninstall uninstall-doc

all: install install-doc

install:
	mkdir -p ${INSTALL_FONT}
	cp -a tjpborders-light/${VERSION}/TJPBorders-Light.otf ${INSTALL_FONT}/
	cp -a tjpborders-medium/${VERSION}/TJPBorders-Medium.otf ${INSTALL_FONT}/
	# cp -a tjpborders-bold/${VERSION}/TJPBorders-Bold.otf ${INSTALL_FONT}/

install-doc:
	mkdir -p ${INSTALL_DOC}
	cp -a FONTLOG.txt ${INSTALL_DOC}/
	cp -a OFL-FAQ ${INSTALL_DOC}/
	cp -a OFL.txt ${INSTALL_DOC}/
	cp -a README.md ${INSTALL_DOC}/

uninstall:
	rm -rf ${INSTALL_FONT}/

uninstall-doc:
	rm -rf ${INSTALL_DOC}/
