# TJP Borders font family
This file provides detailed information on the TJP Borders family of fonts.
This information should be distributed along with the TJP Borders fonts and any derivative works.


## Basic Font Information
TJP Borders Light and TJP Borders Medium are the two fonts of TJP Borders.
These fonts were made to be able to construct borders in your documents.
The font consists of squares with a part of a border on them.
The layout is such that the blocks

```
qwe  rty  u o
asd  fgh
zxc  vbn  m .
```

have the borders in the outward direction, with the central letter having a filling form.
These blocks can be used both small and capital and small and capital letters can be used through each other.
See the fontoverview files.

A special note should be given to the `<TAB>` key.
This key can be used for spacing only when the tab size matches the full font height.
The font height is 120% of the font size.
I recommend using spaces though, because they are exactly the right size.

The design files are included in the distribution.

## Distribution and Copying
TJP Borders is available under the SIL Open Font License 1.1.
As long as the terms of the license are met you are free / libre to use, adjust, redistribute the font for your works.
